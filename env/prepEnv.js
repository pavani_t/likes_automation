/**
 * Created by sys1095 on 2/5/17.
 */

var env = require('./env');
var envCurrent = env.getEnvironment();

var prepEnv = {};

prepEnv.getEnvironment = function () {
    return envCurrent;
};


module.exports = prepEnv;

// We can use this file directly in service file, but intentionally writing it
