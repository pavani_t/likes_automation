/**
 * Created by sys1095 on 1/5/17.
 */


var envConstants = {};

var environment = {
    ENV_DEV_HOST: 'ENV_DEV_HOST',
    ENV_DEV_SERVER: 'ENV_DEV_SERVER',
    ENV_TEST_SERVER: 'ENV_TEST_SERVER',
    ENV_PROD_SERVER: 'ENV_PROD_SERVER',
};

connection = {
    CONNECTION_TEST: 'TEST',
    CONNECTION_LIVE: 'LIVE',
};

envConstants.environment = environment;
envConstants.connection = connection;


module.exports = envConstants;