/**
 * Created by sys1095 on 1/5/17.
 */

var logger = require('tracer').colorConsole({
    format: '{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})',
    dateformat: 'dd-mm-yyyy HH:MM:ss TT'
});

var envConstants = require('./envConstants');
var utilities = require('./../utilities/utility');

var environments = Object.keys(envConstants.environment).map(function (key) {
    return envConstants.environment[key];
});
var connections = Object.keys(envConstants.connection).map(function (key) {
    return envConstants.connection[key];
});

var env = {};
var environment;
var connection;

env.setEnvironment = function (env, conn) {
    if (utilities.checkPropInArray(environments, env)) {
        environment = env;
    }
    else {
        environment = envConstants.environment.ENV_TEST_SERVER;
    }

    if (utilities.checkPropInArray(connections, conn)) {
        connection = conn;
    }
    else {
        connection = envConstants.connection.CONNECTION_TEST;
    }
};

env.getEnvironment = function () {
    return {
        environment: environment,
        connection: connection
    };
};

env.envConstants = envConstants;
env._environment = environments;
env._connection = connections;


module.exports = env;