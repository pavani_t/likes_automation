/**
 * Created by sys1095 on 17/4/17.
 */

var config = {};


//config.fileDirPath = 'data/';
config.fileDirPath = 'data/slot_';

// config.iterateInterval = 20000;
config.iterateInterval = 1000;
//config.iterateInterval = 3000;

config.dataRangeLimit = 500;
//config.dataRangeLimit = 1000;
//config.dataRangeLimit = 10;
//config.dataRangeLimit = 1;

//config.dataFetchLimit = 250;
config.dataFetchLimit = 100;
//config.dataFetchLimit = 5;

config.dataRangeSlots = {
    1: {
        from: 0,
        to: 30000000
    },
    2: {
        from: 30000000,
        to: 60000000
    },
    3: {
        from: 60000000,
        to: 90000000
    },
    4: {
        from: 90000000,
        to: 120000000
    },
    5: {
        from: 120000000,
        to: 150000000
    },
    6: {
        from: 150000000,
        to: 180000000
    },
    7: {
        from: 180000000,
        to: 210000000
    },

    //1: {
    //    from: 0,
    //    to: 300
    //},
    //2: {
    //    from: 300,
    //    to: 600
    //},
    //3: {
    //    from: 300,
    //    to: 900
    //},
    //4: {
    //    from: 900,
    //    to: 1200
    //},
    //5: {
    //    from: 1200,
    //    to: 1500
    //},
    //6: {
    //    from: 1500,
    //    to: 1800
    //},
    //7: {
    //    from: 1800,
    //    to: 2100
    //},

};


module.exports = config;