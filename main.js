/**
 * Created by sys1010 on 11/10/18.
 */
/**
 * Created by sys1010 on 16/5/18.
 */
var mongoJs = require('mongojs');
var logger = require('tracer').colorConsole({
    format: '{{timestamp}} [{{title}}] {{message}} (in {{path}}:{{line}})',
    dateformat: 'dd-mm-yyyy HH:MM:ss TT'
});
var vertica = require('vertica');
var mini = require('minimist');

var constants = require('./constants/constants');
var utilities = require('./utilities/utility');
var config = require('./config/config');
var env = require('./env/env');


// var main = require("./main");
var main = require("./io-queue-client-connector/main.js");
var mqChannel;

var iterateInterval = config.iterateInterval;

var elasticIp = '';
var es_index = '';
var es_type = '';

var mongoIp = '';
var mongoUser = '';
var mongoPassword = '';

var mongoIp2 = '';
var mongoUser2 = '';
var mongoPassword2 = '';
// var mongoDBName = 'tags';

var mongoDBName = 'processing_data';
var mongoDBName1 = 'tags';
var mongoDBName2 = 'output_data';
var mongoDBName3 = 'request_data';
var conn;
var verticaProdServer;
var verticaQuery;

var master_like_col = 'master_like';
var likes_master_col = 'likes_master';
var upload_new_data_col = 'upload_new_data';
var upload_new_data_set_col = 'upload_new_data_set';
var counter_col = 'counter';
var data_col = '';
var meta_col = '';
var fetchLimit = 250;


var processing_db;
var tags_db;
var output_data_db;
var request_data_db;
var vertica_db;


initialization();

function initialization() {
    var args = mini(process.argv.slice(2));
    if (utilities.checkPropInObject(args, constants.ENV_RUN)) {
        var e = args[constants.ENV_RUN];
        if (utilities.isString(e) && utilities.stringContains(e, '_')) {
            var currentEnv = e.split('_');
            var environment = currentEnv[0];
            var connection = currentEnv[1];

            //logger.log(environment, connection);
            env.setEnvironment(constants.env[environment], constants.conn[connection]);

            if (utilities.checkPropInObject(args, constants.SLOT)) {
                var slotNumber = args[constants.SLOT];
                if (utilities.isNumber(slotNumber) && slotNumber > 0 && slotNumber < 9) {
                    logger.log('Connecting to ', e, ' & slot ', slotNumber);
                    proceedNext(slotNumber);
                }
                else {
                    logger.error('-s argument is Number (0<s<6); Eg: node file.js -e TEST_TEST -s 1;');
                }
            }
            else {
                logger.error('-s argument is required; Eg: node file.js -e TEST_TEST -s 1;');
            }
        }
        else {
            logger.error('-e argument is in wrong format; Eg: node file.js -e TEST_TEST -s 1;');
        }
    }
    else {
        logger.error('-e argument is required; Eg: node file.js -e TEST_TEST -s 1;');
    }
}


function proceedNext(slotNumber) {
    var currentEnv = env.getEnvironment();

    logger.log(currentEnv);

    if (currentEnv.environment === env.envConstants.environment.ENV_PROD_SERVER) {
        mongoIp = '10.29.0.176';
        mongoUser = 'dmpPythonUser';
        mongoPassword = 'eMma.taNtRa';
        // mongoIp2 = '209.58.139.155';
        mongoIp2 = '10.29.0.171';
        mongoUser2 = 'tpuser';
        mongoPassword2 = 'gaic0n.vy';
        verticaProdServer = {
            host: '10.29.0.166',
            user: 'dbadmin',
            password: 'pr@od$ingK45',
            database: 'subscriber_data'
        }
    }
    else if (currentEnv.environment === env.envConstants.environment.ENV_TEST_SERVER) {
        mongoIp = '103.18.248.31';
        mongoUser = 'dmpUser';
        mongoPassword = 'dRagon.$hoe';
        mongoIp2 = '103.18.248.31';
        mongoUser2 = 'dmpUser';
        mongoPassword2 = 'dRagon.$hoe';
        verticaProdServer = {
            host: '103.18.248.32',
            user: 'user_testing',
            password: 'testing',
            database: 'testing'
        }
    }
    else {
        elasticIp = '103.18.248.31';
        mongoIp = '103.18.248.31';
        mongoUser = 'dmpUser';
        mongoPassword = 'dRagon.$hoe';
    }

    processing_db = getDB();
    tags_db = getDB1();
    output_data_db = getDB2();
    request_data_db = getDB3();
    vertica_db = getVerticaConnection();

    if (!utilities.isUndefinedOrNull(processing_db) && !utilities.isUndefinedOrNull(tags_db) && !utilities.isUndefinedOrNull(output_data_db) && !utilities.isUndefinedOrNull(request_data_db) && vertica_db) {
        // logger.log(processing_db, tags_db, output_data_db, request_data_db, vertica_db);
        init();
    }
}


function getDB() {
    if (processing_db) {
        return processing_db;
    }
    else {
        processing_db = getMongoConnection();
        logger.log('getMongoConnection', processing_db);
        return processing_db;
    }
}

function getDB1() {
    if (tags_db) {
        return tags_db;
    }
    else {
        tags_db = getMongoConnection1();
        return tags_db;
    }
}

function getDB2() {
    if (output_data_db) {
        return output_data_db;
    }
    else {
        output_data_db = getMongoConnection2();
        return output_data_db;
    }
}

function getDB3() {
    if (request_data_db) {
        return request_data_db;
    }
    else {
        request_data_db = getMongoConnection3();
        return request_data_db;
    }
}

function getMongoConnection() {
    var _config = {};
    _config.hostName = mongoIp;
    _config.port = '27017';
    _config.user = mongoUser;
    _config.pass = mongoPassword;
    _config.dbName = mongoDBName;
    //var url = 'mongodb://' + _config.hostName + ':' + _config.port + '/' + _config.dbName;
    var url = 'mongodb://' + _config.user + ':' + _config.pass + '@' +
        _config.hostName + ':' + _config.port + '/' + _config.dbName;
    // logger.log(url);
    return mongoJs(url);
}

function getMongoConnection1() {
    var _config = {};
    _config.hostName = mongoIp;
    _config.port = '27017';
    _config.user = mongoUser;
    _config.pass = mongoPassword;
    _config.dbName = mongoDBName1;
    //var url = 'mongodb://' + _config.hostName + ':' + _config.port + '/' + _config.dbName;
    var url = 'mongodb://' + _config.user + ':' + _config.pass + '@' +
        _config.hostName + ':' + _config.port + '/' + _config.dbName;
    // logger.log(url);
    return mongoJs(url);
}

function getMongoConnection2() {
    var _config = {};
    _config.hostName = mongoIp2;
    _config.port = '27017';
    _config.user = mongoUser2;
    _config.pass = mongoPassword2;
    _config.dbName = mongoDBName2;
    //var url = 'mongodb://' + _config.hostName + ':' + _config.port + '/' + _config.dbName;
    var url = 'mongodb://' + _config.user + ':' + _config.pass + '@' +
        _config.hostName + ':' + _config.port + '/' + _config.dbName + "?authSource=admin";
    // logger.log(url);
    return mongoJs(url);
}

function getMongoConnection3() {
    var _config = {};
    _config.hostName = mongoIp;
    _config.port = '27017';
    _config.user = mongoUser;
    _config.pass = mongoPassword;
    _config.dbName = mongoDBName3;
    //var url = 'mongodb://' + _config.hostName + ':' + _config.port + '/' + _config.dbName;
    var url = 'mongodb://' + _config.user + ':' + _config.pass + '@' +
        _config.hostName + ':' + _config.port + '/' + _config.dbName;
    // logger.log(url);
    return mongoJs(url);
}

function getVerticaConnection() {
    conn = vertica.connect(verticaProdServer);
    return conn;
}

function init() {
    logger.log('------------------------- init() -------------------------');
    startProcess();
}

function startProcess() {
    var find = {};
    find.status = 0;
    var fields = {};
    var limit = {limit: 1};
    // logger.log(upload_new_data_col, find, fields);
    request_data_db.collection(upload_new_data_col).find(find, fields).sort({"_id": 1}).limit(1, function (err, docs) {
        // logger.log(err, docs, request_data_db);
        if (err) {
            logger.error(err);
        }
        else {
            //logger.log(docs);
            operateOnDocs(docs);
        }
    });
}

function get_channel(likes, upd_id) {
    main.getChannel(function (channel) {
        mqChannel = channel;
        startProcessPreNew(likes, channel, upd_id);
        // startProcessNew(likes, channel, upd_id);
        // send();
    });
}


function send(data, mqChannel, next) {
    var clientId = 97;
    // logger.log(clientId, mqChannel, data);
    main.sendData(clientId, mqChannel, data, function (err, response) {
        // logger.log(err, response);
        return next(err, response);
    });
}


function mongo_find(mongodb, col, find, limit, next) {
    mongodb.collection(col).find(find, {}, limit, function (err, docs) {
        if (err) {
            logger.error(err);
        }
        else {
            //logger.log(docs);
            next(docs)
        }
    });
}


function getNextSequence(db_param, name, next) {
    var find = {};
    find['_id'] = name;
    var update = {};
    update["seq"] = 1;
    var fields = {$inc: update};
    if (!utilities.isUndefinedOrNull(processing_db)) {
        db_param.collection('counter').findAndModify({
            query: find,
            update: fields,
            new: true,
            upsert: true
        }, function (err, doc) {
            // logger.log(err, doc);
            if (doc) {
                next(err, doc.seq);
            }
            find = null;
            update = null;
            fields = null;
        });
    }
    else {
        logger.log('mongo client not created');
    }
}


function mongo_insert(mongodb, col, fields, next) {
    mongodb.collection(col).insert(fields, function (err, doc) {
        if (err) {
            logger.error('Error while inserting ', fields);
            // exit()
        }
        else {
            logger.log('inserted record in mongo', fields);
        }
        next(err, doc);
    });
}

function getPresentTimeInSecs() {
    var d = new Date();
    return parseInt(Math.floor(d.getTime() / 1000));
}


function operateOnDocs(docs) {
    // console.log(docs);
    var doneCount = 0;
    if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
        var doc = docs[0];
        var pag_name = doc['mName'];
        var mid = doc['mId'];
        var cat_name = 'dmp_custom_tag_' + pag_name;
        var _find_category = {'name': cat_name, 'l1': 'none', 'level': 1};
        var upd_id = doc['_id'];
        // var likes = {cat_name: [pag_name]};
        var likes = [];
        var likes1 = {};
        likes1["category_key"] = cat_name;
        likes1["values"] = [pag_name];
        likes[0] = likes1;
        // logger.log(likes);
        mongo_find(tags_db, likes_master_col, _find_category, 10, function (docs) {
            // logger.log(tags_db.toString(), likes_master_col, _find_category, docs);
            if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
                pageOperations(pag_name, cat_name, mid, function (err, res) {
                    if (err) {
                        logger.log(err);
                        checkDone(likes, upd_id)
                    }
                    else if (res) {
                        checkDone(likes, upd_id)
                    }
                })
            } else {
                // logger.log("testingggg");
                getNextSequence(tags_db, likes_master_col, function (err, res) {
                    var id = res;
                    var insert_obj_cat = {"_id": id, "l1": 'none', "level": 1, "name": cat_name};
                    mongo_insert(tags_db, likes_master_col, insert_obj_cat, function (err, res) {
                        if (err) {
                            logger.log(err);
                        }
                        else if (res) {
                            pageOperations(pag_name, cat_name, mid, function (err, res) {
                                if (err) {
                                    logger.log(err);
                                    checkDone(likes, upd_id)
                                }
                                else if (res) {
                                    checkDone(likes, upd_id)
                                }
                            })
                        }

                    })
                });
            }

        });
    }
    else {
        logger.log('No docs found, waiting for 10 sec to start again');
        setTimeout(function () {
            startProcess();
        }, iterateInterval);
        //logger.log('No docs found, Might be Done');
        //exit();
    }
    function checkDone(likes, id) {
        // logger.log(likes);
        // logger.log("im in........");
        var _find = {'_id': id};
        updateRecordStatus(request_data_db, upload_new_data_col, _find, {status: 2}, function (err, res) {
            // logger.log('----------- call back 2-------');
            get_channel(likes, id);
            setTimeout(function () {
                startProcess();
            }, iterateInterval);
        });

    }
}


function pageOperations(pag_name, cat_name, mid, next) {
    var _find_page = {'name': pag_name, 'l1': cat_name, 'level': 0};
    mongo_find(tags_db, likes_master_col, _find_page, 1, function (docs) {
        if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
            var doc_id = docs[0]['_id'];
            var master_like_doc = {"_id": doc_id, "l1": cat_name, "level": 0, "name": pag_name, "mid": mid};
            mongo_find(processing_db, master_like_col, master_like_doc, 1, function (err, res) {
                if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
                    return next(err, res)
                }
                else {
                    mongo_insert(processing_db, master_like_col, master_like_doc, function (err, res) {
                        if (err) {
                            logger.log(err);
                            return next(err, res)
                        }
                        else {
                            verticaInsertion(doc_id, mid, function (err, res) {
                                // logger.log(doc_id, mid, err, res);
                                if (err) {
                                    logger.log(err);
                                    return next(err, res)
                                }
                                else {
                                    return next(err, res)
                                }
                            });
                        }
                    })
                }
            })
        }
        else {
            getNextSequence(tags_db, likes_master_col, function (err, res) {
                var id = res;
                var insert_obj_pag = {"_id": id, "l1": cat_name, "level": 0, "name": pag_name};
                mongo_insert(tags_db, likes_master_col, insert_obj_pag, function (err, res) {
                    if (err) {
                        logger.log(err);
                        return next(err, res)
                    }
                    else if (res) {
                        var master_like_doc = {
                            "_id": id,
                            "l1": cat_name,
                            "level": 0,
                            "name": pag_name,
                            "mid": mid
                        };
                        mongo_find(processing_db, master_like_col, master_like_doc, 1, function (err, res) {
                            if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
                                return next(err, res)
                            }
                            else {
                                mongo_insert(processing_db, master_like_col, master_like_doc, function (err, res) {
                                    if (err) {
                                        logger.log(err);
                                        return next(err, res)
                                    }
                                    else {
                                        verticaInsertion(id, mid, function (err, res) {
                                            // logger.log(id, mid, err, res);
                                            if (err) {
                                                logger.log(err);
                                                return next(err, res)
                                            }
                                            else {
                                                return next(err, res)
                                            }
                                        });

                                    }
                                })
                            }
                        })
                    }
                })
            });
        }
    });

}


function startProcessPreNew(likes, channel, upd_id) {
    var find = {};
    // find.status = 2;
    find.status = 0;
    find.csvId = upd_id;
    // find.status = 3;
    // find.status = 2;
    var fields = {};
    var limit = {limit: fetchLimit};
    // logger.log(master_like_col, find, fields);
    output_data_db.collection(upload_new_data_set_col).find(find, fields, limit, function (err, docs) {
        // logger.log(err, docs, upload_new_data_set_col);
        if (err) {
            logger.error(err);
        }
        else {
            //logger.log(docs);
            operateOnDocsPreNew(likes, docs, channel, upd_id);
        }
    });
}

function operateOnDocsPreNew(likes, docs, channel, upd_id) {
    // logger.log(docs);
    var doneCount = 0;
    if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
        docs.forEach(function (doc) {
            var _find = {_id: doc['_id']};
            PregoAhead(doc, channel, function (err, ress) {
                // logger.log('----------- call back -------');
                if (err) {
                    logger.error('ERROR occurred', err);
                    updateRecordStatus(output_data_db, upload_new_data_set_col, _find, {status: -1}, function (err, res) {
                        // logger.log('----------- call back 1-------');
                        doneCount++;
                        checkDone1(docs);
                    });
                }
                else if (ress) {
                    // logger.log('----------- call back 2.0-------');
                    updateRecordStatus(output_data_db, upload_new_data_set_col, _find, {status: 1}, function (err, res) {
                        // logger.log('----------- call back 2-------');
                        doneCount++;
                        checkDone1(docs);
                    });
                }
            });
        })
    }
    else {
        setTimeout(function () {
            startProcessNew(likes, channel, upd_id);
        }, 900000);
        // startProcessNew(likes, channel, upd_id);
    }
    function checkDone1(docs) {
        if (doneCount === docs.length) {
            logger.log('Chunk done ', doneCount);
            setTimeout(function () {
                startProcessPreNew(likes, channel, upd_id);
                doneCount = 0;
            }, 1000);
        }
    }
}


function PregoAhead(doc, channel, next) {
    var insertObj = {};
    insertObj['email'] = doc['email'];
    send(insertObj, channel, function (err, res) {
        // logger.log(insertObj);
        if (err) {
            logger.log(err);
        }
        else if (res) {
            //console.log(res);
        }
        next(err, res);
    })
}


function startProcessNew(likes, channel, upd_id) {
    var find = {};
    // find.status = 2;
    find.status = 1;
    find.csvId = upd_id;
    // find.status = 3;
    // find.status = 2;
    var fields = {};
    var limit = {limit: fetchLimit};
    // logger.log(master_like_col, find, fields);
    output_data_db.collection(upload_new_data_set_col).find(find, fields, limit, function (err, docs) {
        // logger.log(err, docs, upload_new_data_set_col);
        if (err) {
            logger.error(err);
        }
        else {
            //logger.log(docs);
            operateOnDocsNew(docs, likes, channel, upd_id);
        }
    });
}

function operateOnDocsNew(docs, likes, channel, upd_id) {
    // logger.log(docs);
    var doneCount = 0;
    if (!utilities.isUndefinedOrNullOrEmptyString(docs) && utilities.isArray(docs) && !utilities.isEmptyArray(docs)) {
        docs.forEach(function (doc) {
            var _find = {_id: doc['_id']};
            goAhead(doc, likes, channel, function (err, ress) {
                // logger.log('----------- call back -------');
                if (err) {
                    logger.error('ERROR occurred', err);
                    updateRecordStatus(output_data_db, upload_new_data_set_col, _find, {status: -2}, function (err, res) {
                        // logger.log('----------- call back 1-------');
                        doneCount++;
                        checkDone1(docs);
                    });
                }
                else if (ress) {
                    // logger.log('----------- call back 2.0-------');
                    updateRecordStatus(output_data_db, upload_new_data_set_col, _find, {status: 2}, function (err, res) {
                        // logger.log('----------- call back 2-------');
                        doneCount++;
                        checkDone1(docs);
                    });
                }
            });
        })
    }
    else {
        var _find = {'_id': upd_id};
        updateRecordStatus(request_data_db, upload_new_data_col, _find, {
            status: 3,
            timestamp: getPresentTimeInSecs()
        }, function (err, res) {
            if (err) {
                logger.log(err)
            }
            else if (res) {
                logger.log('csv ' + upd_id + ' completed');
            }
            // logger.log('No docs found, waiting for 10 sec to start again');
            // setTimeout(function () {
            //     startProcessNew(likes, channel, upd_id);
            // }, iterateInterval);
        });
        //logger.log('No docs found, Might be Done');
        //exit();
    }
    function checkDone1(docs) {
        if (doneCount === docs.length) {
            logger.log('Chunk done ', doneCount);
            setTimeout(function () {
                startProcessNew(likes, channel, upd_id);
                doneCount = 0;
            }, 1000);
        }
    }
}


function goAhead(doc, likes, channel, next) {
    var insertObj = {};
    insertObj['email'] = doc['email'];
    insertObj['likes'] = likes;
    send(insertObj, channel, function (err, res) {
        // logger.log(insertObj);
        if (err) {
            logger.log(err);
        }
        else if (res) {
            //console.log(res);
        }
        next(err, res);
    })
}

function verticaInsertion(id, mid, next) {
    var count = 0;
    var values = id + "," + mid + "," + count;
    verticaQuery = "insert into master_like(id,mid,count) values(" + values + ");commit;";
    conn.query(verticaQuery, function (err, res) {
        // logger.log(verticaQuery, err, res);
        return next(err, res);
        // console.log(res.rows)
    });

    // conn.query('select * from master_like where mid=399;commit;', function (err, res) {
    //     logger.log(verticaQuery,err,res);
    //     // console.log(res.rows)
    // })
}

function updateRecordStatus(db_param, col, find, fields, next) {
    var _fields = {$set: fields};
    db_param.collection(col).update(find, _fields, function (err, doc) {
        if (err) {
            logger.error('Error while updating ', find, fields);
            exit()
        }
        else {
            //logger.log('Updated record ', find, fields);
        }
        next(err, doc);
    });
}


function exit() {
    logger.log('------------------------- exit() -------------------------');
    //process.exit(1);
}
