/**
 * Created by sys1095 on 4/4/17.
 */


var constants = {};

constants.ENV_RUN = 'e';
constants.SLOT = 's';

constants.env = {
    ENV_DEV_HOST: 'DEVH',
    ENV_DEV_SERVER: 'DEVS',
    ENV_TEST_SERVER: 'TEST',
    ENV_PROD_SERVER: 'PROD',

    DEVH: 'ENV_DEV_HOST',
    DEVS: 'ENV_DEV_SERVER',
    TEST: 'ENV_TEST_SERVER',
    PROD: 'ENV_PROD_SERVER',
};

constants.conn = {
    CONNECTION_TEST: 'TEST',
    CONNECTION_LIVE: 'LIVE',

    TEST: 'TEST',
    LIVE: 'LIVE',
};

constants.esp = {
    "gmail": 1,
    "googlemail": 1,
    "google": 1,

    "rocketmail": 2,
    "ymail": 2,
    "yahoo": 2,

    "hotmail": 3,
    "live": 3,
    "msn": 3,
    "passport": 3,
    "outlook": 3,

    "aol": 4,
    "love": 4,
    "ygm": 4,
    "games": 4,
    "wow": 4,
};

constants.reactionType = {
    "sent": 0,
    "open": 1,
    "view": 1,
    "click": 2,
    "lead": 3,
    "bounce_s": -3,
    "bounce_h": -4,
    "unsub": -1,
    "spam": -2
}

constants.location = {
    "current": "current",
    "native": "native",
    "work": "work",
};


module.exports = constants;